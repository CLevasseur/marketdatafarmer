using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MarketDataFarmer.WebSocket
{
    public class DeribitClient
    {
        private Uri _apiUri;

        public DeribitClient(Uri apiUri)
        {
            _apiUri = apiUri;
        }

        public async Task Subscribe(List<string> channels, BlockingCollection<JsonRpcResponse> responseQueue)
        {
            using (var cws = new ClientWebSocket())
            {
                using (var cts = new CancellationTokenSource())
                {
                    await cws.ConnectAsync(_apiUri, cts.Token);

                    var subscriptionRequest = new JsonRpcRequest(
                        "public/subscribe",
                        new JsonRpcRequestParams(channels),
                        "2.0",
                        0
                    );

                    var encoded = Encoding.UTF8.GetBytes(subscriptionRequest.AsJsonString());
                    new ArraySegment<Byte>(encoded, 0, encoded.Length);
                    Console.Out.WriteLine($"Susbcribing to websocket channels {String.Join(", ", channels)}");
                    await cws.SendAsync(encoded, WebSocketMessageType.Text, true, cts.Token);

                    while (true)
                    {
                        ArraySegment<Byte> receiveBuffer = new ArraySegment<byte>(new Byte[8192]);

                        using (var ms = new MemoryStream())
                        {
                            WebSocketReceiveResult result;
                            do
                            {
                                result = await cws.ReceiveAsync(receiveBuffer, cts.Token);
                                ms.Write(receiveBuffer.Array, receiveBuffer.Offset, result.Count);
                            }
                            while (!result.EndOfMessage);

                            ms.Seek(0, SeekOrigin.Begin);

                            using (var reader = new StreamReader(ms, Encoding.UTF8))
                            {
                                try
                                {
                                    var message = reader.ReadToEnd();
                                    var jsonRpcResponse = JsonRpcResponse.FromJsonString(message);
                                    responseQueue.Add(jsonRpcResponse, cts.Token);
                                }
                                catch (JsonSerializationException)
                                {
                                    // Some messages need to be ignored because they're not ticker messages and can't be deserialized here
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}