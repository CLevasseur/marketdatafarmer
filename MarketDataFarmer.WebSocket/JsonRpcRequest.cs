using System.Collections.Generic;
using System.Text.Json;

namespace MarketDataFarmer.WebSocket
{
    public class JsonRpcRequest
    {
        public JsonRpcRequest(string method, JsonRpcRequestParams @params, string jsonrpc, int id)
        {
            this.method = method;
            this.@params = @params;
            this.jsonrpc = jsonrpc;
            this.id = id;
        }

        public string method { get; set; }
        public JsonRpcRequestParams @params { get; set; }
        public string jsonrpc { get; set; }
        public int id { get; set; }

        public string AsJsonString()
        {
            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
            return JsonSerializer.Serialize(this, serializeOptions);
        }
    }

    public class JsonRpcRequestParams
    {
        public JsonRpcRequestParams(List<string> channels)
        {
            this.channels = channels;
        }

        public List<string> channels { get; set; }
    }


}