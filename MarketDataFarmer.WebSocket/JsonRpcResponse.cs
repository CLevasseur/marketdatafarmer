using System.Text.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace MarketDataFarmer.WebSocket
{
    public class Stats
    {
        public double volume_usd { get; set; }
        public double volume { get; set; }
        public double price_change { get; set; }
        public double low { get; set; }
        public double high { get; set; }
    }

    public class Data
    {
        public long timestamp { get; set; }
        public Stats stats { get; set; }
        public string state { get; set; }
        public double settlement_price { get; set; }
        public int open_interest { get; set; }
        public double min_price { get; set; }
        public double max_price { get; set; }
        public double mark_price { get; set; }
        public double last_price { get; set; }
        public string instrument_name { get; set; }
        public double index_price { get; set; }
        public double funding_8h { get; set; }
        public double estimated_delivery_price { get; set; }
        public double current_funding { get; set; }
        public double best_bid_price { get; set; }
        public double best_bid_amount { get; set; }
        public double best_ask_price { get; set; }
        public double best_ask_amount { get; set; }
    }

    public class Params
    {
        public Data data { get; set; }
        public string channel { get; set; }
    }

    [JsonObject(ItemRequired=Required.Always)]
    public class JsonRpcResponse
    {
        public Params @params { get; set; }
        public string method { get; set; }
        public string jsonrpc { get; set; }

        public static JsonRpcResponse FromJsonString(string jsonString)
        {
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            return JsonConvert.DeserializeObject<JsonRpcResponse>(jsonString, new JsonSerializerSettings
            {
                ContractResolver = contractResolver
            });
        }
    }
}