﻿using MarketDataFarmer.DataStore;
using Npgsql;

namespace MarketDataFarmer.DatabaseListener.Console
{
    class Program
    {
        // This program listen to a PostgreSQL new_tick channel which raises an event everytime some ticks are
        // inserted in the ticks table and just print them out
        static void Main(string[] args)
        {
            var dataStore = TimescaleDataStore.FromEnvironmentVariables();
            var conn = dataStore.GetNewConnection();
            conn.Notification += (o, e) =>
            {
                System.Console.Out.WriteLine(e.Payload);
            };
            using (var cmd = new NpgsqlCommand("LISTEN new_tick", conn)) {
                cmd.ExecuteNonQuery();
            }

            while (true) {
                conn.Wait();   // Thread will block here
            }
        }
    }
}