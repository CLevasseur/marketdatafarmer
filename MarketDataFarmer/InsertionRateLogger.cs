using System;
using System.Collections.Generic;

namespace MarketDataFarmer
{
    internal class InsertionRateLogger
    {
        private Queue<DateTimeOffset> _queue;
        private DateTimeOffset _lastLogTime;

        public InsertionRateLogger()
        {
            _queue = new Queue<DateTimeOffset>();
            _lastLogTime = DateTimeOffset.Now;
        }

        public void AddOne()
        {
            var now = DateTimeOffset.Now;
            _queue.Enqueue(now);

            while (now - _queue.Peek() > TimeSpan.FromSeconds(1))
            {
                _queue.Dequeue();
            }

            if (now - _lastLogTime > TimeSpan.FromSeconds(1))
            {
                _lastLogTime = now;
                Console.Out.WriteLine($"{now}: Inserted {_queue.Count} ticks over the past second");
            }

        }
    }
}