﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MarketDataFarmer.WebSocket;
using MarketDataFarmer.DataStore;

namespace MarketDataFarmer
{
    class Program
    {
        static Task Run(string[] args)
        {
            // The data store is the TimescaleDB that will contain all the ticks we got from the websocket
            var dataStore = TimescaleDataStore.FromEnvironmentVariables();
            var client = new DeribitClient(new Uri("ws://www.deribit.com/ws/api/v2/"));

            // The .raw interval is forced here for the sake of the homework, this could be interesting to allow
            // user to specify the .100ms interval
            var tickers = args.Select(arg => "ticker." + arg + ".raw").ToList();
            var responseQueue = new BlockingCollection<JsonRpcResponse>();

            // Read responses from the deribit websocket in a separate thread
            async void Start() => await client.Subscribe(tickers, responseQueue);
            var subscriptionThread = new Thread(Start);
            subscriptionThread.Start();

            var insertionRateLogger = new InsertionRateLogger();

            // Save all ticks received on the websocket in the main thread
            while (true)
            {
                // Inserts are one tick by one tick, see README.md for future improvement that include batch inserts
                // by taking all elements in the queue and inserting them at once
                var jsonRpcResponse = responseQueue.Take();
                dataStore.SaveTickerChannelResponse(jsonRpcResponse);
                insertionRateLogger.AddOne();
            }
        }

        static void Main(string[] args)
        {
            Run(args).GetAwaiter().GetResult();
        }
    }
}
