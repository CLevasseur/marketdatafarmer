CREATE TABLE ticks(
    tick_idx                 BIGINT GENERATED ALWAYS AS IDENTITY,
    timestamp                TIMESTAMP NOT NULL,
    stats_volume_usd         NUMERIC NOT NULL,
    stats_volume             NUMERIC NOT NULL,
    stats_price_change       NUMERIC NOT NULL,
    stats_low                NUMERIC NOT NULL,
    stats_high               NUMERIC NOT NULL,
    state                    TEXT NOT NULL,
    settlement_price         NUMERIC NOT NULL,
    open_interest            NUMERIC  NOT NULL,
    min_price                NUMERIC NOT NULL,
    max_price                NUMERIC NOT NULL,
    mark_price               NUMERIC NOT NULL,
    last_price               NUMERIC NOT NULL,
    instrument_name          TEXT NOT NULL,
    index_price              NUMERIC NOT NULL,
    funding_8h               NUMERIC NOT NULL,
    estimated_delivery_price NUMERIC NOT NULL,
    current_funding          NUMERIC  NOT NULL,
    best_bid_price           NUMERIC NOT NULL,
    best_bid_amount          INTEGER  NOT NULL,
    best_ask_price           INTEGER  NOT NULL,
    best_ask_amount          INTEGER  NOT NULL
);

CREATE OR REPLACE FUNCTION notify_new_tick()
	RETURNS trigger AS
$$
BEGIN
	PERFORM pg_notify('new_tick', to_jsonb(NEW)::text);
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER new_tick
    AFTER INSERT ON ticks
    FOR EACH ROW
    EXECUTE PROCEDURE notify_new_tick();

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
SELECT create_hypertable('ticks', 'timestamp');