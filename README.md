## Architecture

The solution has separate projects for:

- console app to subscribe to Deribit websocket
- another console app to listen to database inserts and print them
- websocket management
- data store/persistence layer for ticks

The `scripts` folder only contains the initialization script for the database. The database docker image will run this script only the first time.

I have decided to use TimescaleDB as the persistence layer for this project because simple files are missing indexes and regular database are optimized for features that tend to be unused in the case of timeseries data. TimescaleDB is a Postgresql extension that optimizes insertion/aggregation of append-only data where a time column only increases.

# How to run 

Note that I don't have a windows machine at home, hopefully the PowerShell syntax doesn't differ too much from the one I used here to run it on MacOS.

## Run the TimescaleDB
```bash
# Run database container in docker
SOLUTION_FOLDER=$(pwd)  # assumes current workdir is at the solution root folder
docker run -v ${SOLUTION_FOLDER}/scripts/db:/docker-entrypoint-initdb.d/ -v tsdb_data:/var/lib/postgresql/data --name timescaledb -p 5432:5432  -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password timescale/timescaledb:latest-pg13
```

## Subscribe to Deribit ticker channel and insert in DB
```bash
DB_HOST=localhost DB_USER=postgres DB_PASSWORD=password DB_PORT=5432 dotnet run --project MarketDataFarmer BTC-PERPETUAL ETH-PERPETUAL
```

You should see something like this:

```
Susbcribing to websocket channels ticker.BTC-PERPETUAL.raw, ticker.ETH-PERPETUAL.raw
10/31/2021 12:19:14 +01:00: Inserted 30 ticks over the past second
10/31/2021 12:19:15 +01:00: Inserted 21 ticks over the past second
10/31/2021 12:19:16 +01:00: Inserted 4 ticks over the past second
10/31/2021 12:19:17 +01:00: Inserted 5 ticks over the past second
10/31/2021 12:19:18 +01:00: Inserted 9 ticks over the past second
```

## Listen to TimescaleDB inserts and print ticks in console
```bash
DB_HOST=localhost DB_USER=postgres DB_PASSWORD=password DB_PORT=5432 dotnet run --project MarketDataFarmer.DatabaseListener.Console
```

You should see inserted tick printed like so:

```
{"state": "open", "tick_idx": 707, "max_price": 61794.06, "min_price": 59967.63, "stats_low": 60425.5, "timestamp": "2021-10-31T11:20:13.011", "funding_8h": 0.00019102, "last_price": 60880.5, "
: 60881.66, "stats_high": 62572.5, "index_price": 60816.95, "stats_volume": 5190.14421974, "open_interest": 749800460, "best_ask_price": 60882, "best_bid_price": 60882, "best_ask_amount": 78100
_amount": 15710, "current_funding": 0.00056401, "instrument_name": "BTC-PERPETUAL", "settlement_price": 61422, "stats_volume_usd": 319177869.99, "stats_price_change": -1.4895, "estimated_delive
60816.95}
{"state": "open", "tick_idx": 708, "max_price": 61791.76, "min_price": 59965.4, "stats_low": 60425.5, "timestamp": "2021-10-31T11:20:13.158", "funding_8h": 0.00019102, "last_price": 60880.5, "m
 60879.4, "stats_high": 62572.5, "index_price": 60814.69, "stats_volume": 5190.14421974, "open_interest": 749800460, "best_ask_price": 60882, "best_bid_price": 60882, "best_ask_amount": 78100, 
mount": 15710, "current_funding": 0.00056405, "instrument_name": "BTC-PERPETUAL", "settlement_price": 61422, "stats_volume_usd": 319177869.99, "stats_price_change": -1.4895, "estimated_delivery
814.69}
{"state": "open", "tick_idx": 709, "max_price": 4314.17, "min_price": 4186.66, "stats_low": 4205.05, "timestamp": "2021-10-31T11:20:13.158", "funding_8h": 0.000087, "last_price": 4249.8, "mark_
0.56, "stats_high": 4401.3, "index_price": 4249.18, "stats_volume": 52839.690412, "open_interest": 248266594, "best_ask_price": 4251, "best_bid_price": 4250.7, "best_ask_amount": 57513, "best_b
 6968, "current_funding": 0, "instrument_name": "ETH-PERPETUAL", "settlement_price": 4321.56, "stats_volume_usd": 227678305, "stats_price_change": -2.2248, "estimated_delivery_price": 4249.18}
```

## Things to do or improve

- Using something like Entity Framework to map JsonRpcResponse fields to database columns instead of manually coding the SQL query, this would make the TimescaleDb.cs much smaller/cleaner.
- Inserts are done tick by tick, if the DB is slower to insert than the rate at which ticks are received this would result in an ever increasing lag between tick timestamp and tick insertion date, memory would also explode if the queue got too big. Batch inserts would solve this issue.
- Improve error handling
- Retry on connection lost to deribit
- If connection to DB is lost during SQL query, then the program crashes instead of reconnecting and retrying
- No graceful kill on CTRL-C (Cleanly drop websocket and connection to database)
- Add tests and CI
- Add orchestration around these processes (kubernetes, docker-compose) instead of running service one my one manually
- Add new routes in DeribitClient
