﻿using System;
using System.Data;
using MarketDataFarmer.WebSocket;
using Npgsql;

namespace MarketDataFarmer.DataStore
{
    public class TimescaleDataStore : IDataStore
    {
        // As mentioned in the README.md, we could define a mapping between JsonRpcResponse fields and the DB columns
        // and use a tool such as Entity Framework to write this query instead of doing it manually. This would also
        // allow for batch insert without having to deal with string concatenation/interpolation here
        private static string insertQuery = @"INSERT INTO ticks (timestamp, stats_volume_usd, stats_volume, stats_price_change, stats_low, stats_high, state, settlement_price, open_interest, min_price, max_price, mark_price, last_price, instrument_name, index_price, funding_8h, estimated_delivery_price, current_funding, best_bid_price, best_bid_amount, best_ask_price, best_ask_amount) 
VALUES ( @timestamp, @stats_volume_usd, @stats_volume, @stats_price_change, @stats_low, @stats_high, @state, @settlement_price, @open_interest, @min_price, @max_price, @mark_price, @last_price, @instrument_name, @index_price, @funding_8h, @estimated_delivery_price, @current_funding, @best_bid_price, @best_bid_amount, @best_ask_price, @best_ask_amount)";

        private readonly string _connectionString;
        private NpgsqlConnection _connection;

        public TimescaleDataStore(string host, string user, string password, string port)
        {
            _connectionString = String.Format("Server={0};Username={1};Database=postgres;Port={2};Password={3}",
                host,
                user,
                port,
                password);
        }

        public static TimescaleDataStore FromEnvironmentVariables()
        {
            var host = ForceGetEnvironmentVariable("DB_HOST");
            var user = ForceGetEnvironmentVariable("DB_USER");
            var password = ForceGetEnvironmentVariable("DB_PASSWORD");
            var port = ForceGetEnvironmentVariable("DB_PORT");

            return new TimescaleDataStore(host, user, password, port);
        }

        private static string ForceGetEnvironmentVariable(string key)
        {
            var value = Environment.GetEnvironmentVariable(key);
            if (value == null)
            {
                throw new NoNullAllowedException($"Env var {key} is missing");
            }

            return value;
        }

        private NpgsqlConnection GetConnection()
        {
            if (_connection != null && _connection.State != ConnectionState.Closed) return _connection;

            _connection = GetNewConnection();

            return _connection;
        }

        public NpgsqlConnection GetNewConnection()
        {
            _connection = new NpgsqlConnection(_connectionString);
            _connection.Open();
            return _connection;
        }

        public void SaveTickerChannelResponse(JsonRpcResponse response)
        {
            using (var command = new NpgsqlCommand(insertQuery, GetConnection()))
            {
                command.Parameters.AddWithValue("timestamp", DateTimeOffset.FromUnixTimeMilliseconds(response.@params.data.timestamp));
                command.Parameters.AddWithValue("stats_volume_usd", response.@params.data.stats.volume_usd);
                command.Parameters.AddWithValue("stats_volume", response.@params.data.stats.volume);
                command.Parameters.AddWithValue("stats_price_change", response.@params.data.stats.price_change);
                command.Parameters.AddWithValue("stats_low", response.@params.data.stats.low);
                command.Parameters.AddWithValue("stats_high", response.@params.data.stats.high);
                command.Parameters.AddWithValue("state", response.@params.data.state);
                command.Parameters.AddWithValue("settlement_price", response.@params.data.settlement_price);
                command.Parameters.AddWithValue("open_interest", response.@params.data.open_interest);
                command.Parameters.AddWithValue("min_price", response.@params.data.min_price);
                command.Parameters.AddWithValue("max_price", response.@params.data.max_price);
                command.Parameters.AddWithValue("mark_price", response.@params.data.mark_price);
                command.Parameters.AddWithValue("last_price", response.@params.data.last_price);
                command.Parameters.AddWithValue("instrument_name", response.@params.data.instrument_name);
                command.Parameters.AddWithValue("index_price", response.@params.data.index_price);
                command.Parameters.AddWithValue("funding_8h", response.@params.data.funding_8h);
                command.Parameters.AddWithValue("estimated_delivery_price", response.@params.data.estimated_delivery_price);
                command.Parameters.AddWithValue("current_funding", response.@params.data.current_funding);
                command.Parameters.AddWithValue("best_bid_price", response.@params.data.best_bid_price);
                command.Parameters.AddWithValue("best_bid_amount", response.@params.data.best_bid_amount);
                command.Parameters.AddWithValue("best_ask_price", response.@params.data.best_ask_price);
                command.Parameters.AddWithValue("best_ask_amount", response.@params.data.best_ask_amount);

                var unused = command.ExecuteNonQuery();
            }
        }
    }
}